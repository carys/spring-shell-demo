package com.xinyues.shell.factory;

public class HaierFactory implements IFactory{

	@Override
	public IComputer createComputer() {
		return new HaierComputer();
	}

	@Override
	public ITelevision createTelevision() {
		return new HaierTelevision();
	}

}
