package com.xinyues.shell.factory;
//抽象工厂
public interface IFactory {
	//创建电脑
	IComputer createComputer();
	//创建电视
	ITelevision createTelevision();
}
