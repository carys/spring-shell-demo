package com.xinyues.shell.factory;

public class GeliFactory implements IFactory{

	@Override
	public IComputer createComputer() {
		return new GeliComputer();
	}

	@Override
	public ITelevision createTelevision() {
		return new GeliTelevision();
	}

}
