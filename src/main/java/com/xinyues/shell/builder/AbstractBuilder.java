package com.xinyues.shell.builder;

public abstract class AbstractBuilder {

	protected Product product = new Product();
	
	public abstract void buildA();
	public abstract void buildB();
	public Product getResult() {
		return product;
	}
}
