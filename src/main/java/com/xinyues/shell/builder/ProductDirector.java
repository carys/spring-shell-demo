package com.xinyues.shell.builder;

public class ProductDirector {

	private AbstractBuilder builder;
	
	public ProductDirector(AbstractBuilder builder) {
		this.builder = builder;
	}
	public Product build() {
		builder.buildB();
		builder.buildA();
		// 如果有n个步骤，依次添加即可。
		return builder.getResult();
	}
	public static void main(String[] args) {
		AbstractBuilder builder = new ProductBuilder();
		ProductDirector director = new ProductDirector(builder);
		Product product = director.build();
		System.out.println(product.getPartA());
	}
}
