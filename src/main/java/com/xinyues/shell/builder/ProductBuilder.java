package com.xinyues.shell.builder;

public class ProductBuilder extends AbstractBuilder{

	@Override
	public void buildA() {
		product.setPartA("组装A部分");
	}

	@Override
	public void buildB() {
		product.setPartB("组装B部分");
	}

}
