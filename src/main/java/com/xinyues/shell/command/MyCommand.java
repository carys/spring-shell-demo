package com.xinyues.shell.command;

import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class MyCommand {
	private boolean connected;
	
	@ShellMethod("连接服务器，格式：connect ip port")
	public String connect(String ip,int port) {
		connected = true;
		return String.format("连接服务成功：%s:%s", ip,port);
	}
	@ShellMethod("连接服务器，格式：connect ip port")
	public String connectDefault(@ShellOption(defaultValue = "localhost")String ip,@ShellOption(defaultValue = "8080")int port) {
		return String.format("连接服务成功：%s:%s", ip,port);
	}
	@ShellMethod(value = "登陆服务器，格式：login-server playerId",key = "login-server")
	public String login(String playerId) {
		return "登陆成功：" + playerId;
	}
	@ShellMethod("Download the nuclear codes.")
    public void download() {
       
    }

    public Availability downloadAvailability() {
        return connected
            ? Availability.available()
            : Availability.unavailable("you are not connected");
    }
	
    @ShellMethod("Add Numbers.")
    public float add(@ShellOption(arity=3) float[] numbers) {
            return numbers[0] + numbers[1] + numbers[2];
    }
	
	
}
