package com.xinyues.shell.shop;

import java.util.List;

public interface IShop {
	/**
	 * 检测购买的条件
	 * @param goodsId
	 * @return
	 */
	boolean checkBuyCondition(String goodsId);
	/** 
	 * 购买某个端口
	 * @param goodsId
	 */
	void buyGoods(String goodsId);
	/**
	 * 获取商店所有的端口
	 * @return
	 */
	List<String> getGoods();
	/**
	 * 此商店是否可以显示
	 * @return
	 */
	boolean isShow();
}
