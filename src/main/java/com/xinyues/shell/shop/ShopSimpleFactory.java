package com.xinyues.shell.shop;

public class ShopSimpleFactory {
		
	public IShop createShop(ShopEnum shopType) {
		IShop shop = null;
		switch (shopType) {
		case SHOP_A:
			shop = new ShopA();
			break;
		case SHOP_B:
			shop = new ShopB();
			break;
		case SHOP_C:
			shop = new ShopC();
			break;

		default:
			break;
		}
		return shop;
	}

}
